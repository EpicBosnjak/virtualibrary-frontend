import { Injectable } from '@angular/core';

import {SimpleBook} from '../model/book';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from './../../environments/environment';
import { BehaviorSubject, Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SimpleBookService {
  baseURL = environment.url;
  dataSimpleBook: BehaviorSubject<SimpleBook[]> = new BehaviorSubject<SimpleBook[]>([]);
  
  constructor(private httpClient: HttpClient) {}


  public getBooks(): Observable<SimpleBook[]>{
    //
    
    this.httpClient.get<SimpleBook[]>(this.baseURL + "books/")
         .subscribe(response =>{
        
          this.dataSimpleBook.next(response);
        
        },
          (error: HttpErrorResponse)=>{
              console.log(error.name + ' ' +error.message)
          });
   
    return this.dataSimpleBook.asObservable();
  }

}
