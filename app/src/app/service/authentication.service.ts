import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { User } from '../model/user'
import { LoginUserDTO } from '../model/dto/loginUserDTO';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  baseurl = environment.url
  public currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;

  constructor(private http: HttpClient) {
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  login(email: string, password: string) {
    return this.http.post<any>(this.baseurl + 'auth/login', { email, password }).pipe(map(user => {
      localStorage.setItem('currentUser', JSON.stringify(user));
      this.currentUserSubject.next(user);
      return user;
    }));
  }
  logout() {
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  isLoggedIn() {
    return localStorage.getItem('currentUser') !== null;
  }

  isAdmin() {
    if (this.isLoggedIn())
      return this.currentUserSubject.value.role == "ADMIN";
  }

  dataUser: BehaviorSubject<User> = new BehaviorSubject<User>(null);
  
  insertBookmarks(bookmarks:number): Observable<User>{
    this.http.put<any>(this.baseurl+"users",{"userId":this.currentUserValue.id,"bookMark":bookmarks}).subscribe(data =>{
      var u = this.currentUserValue;
      u.bookmark+=bookmarks;
      localStorage.setItem('currentUser', JSON.stringify(u));
      this.dataUser.next(data);
    })
    return this.dataUser.asObservable();
  }



}
