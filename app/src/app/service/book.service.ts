import { Injectable } from '@angular/core';

import { Book, SimpleBook } from '../model/book';
import { APIErrorResponse } from '../model/APIError';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from './../../environments/environment';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { InsertBookDTO } from '../model/dto/insertBookDTO';

@Injectable({
  providedIn: 'root'
})
export class BookService {

  baseURL = environment.url;
  dataSimpleBook: BehaviorSubject<SimpleBook[]> = new BehaviorSubject<SimpleBook[]>([]);
  dataBook: BehaviorSubject<Book> = new BehaviorSubject<Book>(null);

  dataInsertBook: BehaviorSubject<Book> = new BehaviorSubject<Book>(null);
  constructor(private httpClient: HttpClient) { }

  public getBooks(): Observable<SimpleBook[]> {
    this.httpClient.get<SimpleBook[]>(this.baseURL + "books/")
      .subscribe(response => {

        this.dataSimpleBook.next(response);

      },
        (error: APIErrorResponse) => {
          var message = '['+ error.error.status + '] ' +error.error.message;
          alert(message);
        });

    return this.dataSimpleBook.asObservable();
  }

  public getBook(id: any): Observable<Book> {
    this.httpClient.get<Book>(this.baseURL + "books/" + id)
      .subscribe(response => {
        this.dataBook.next(response);
      }, (error: APIErrorResponse) => {
        var message = '['+ error.error.status + '] ' +error.error.message;
        alert(message);
       
        
      }
      );

    return this.dataBook.asObservable();
  }

  public insertBook(insertBook: InsertBookDTO):Observable<Book>{
    this.httpClient.post<Book>(this.baseURL + "books",insertBook).subscribe(response => {
      this.dataInsertBook.next(response);
    }, (error: APIErrorResponse) => {
      var message = '['+ error.error.status + '] ' +error.error.message;
      alert(message);
    });
    return this.dataInsertBook.asObservable();
  }

  public insertPDF(bookId:number, file: FormData){
    this.httpClient.post<any>(this.baseURL + "files/"+bookId,file).subscribe(data=>{
      alert("Success, please wait");
    }, (error: APIErrorResponse) => {
      var message = '['+ error.error.status + '] ' +error.error.message;
      alert(message);
    })
  }
  
  public filter(categoryId:number): Observable<SimpleBook[]> {
    this.httpClient.get<SimpleBook[]>(this.baseURL + "books/filter/" + categoryId)
      .subscribe(response => {

        this.dataSimpleBook.next(response);

      },
        (error: APIErrorResponse) => {
          var message = '['+ error.error.status + '] ' +error.error.message;
          alert(message);
        });

    return this.dataSimpleBook.asObservable();
  }


}
