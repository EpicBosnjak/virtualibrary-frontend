import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from '../../environments/environment';
import { InsertUserDTO } from '../model/dto/insertUserDTO';
import { AuthenticationService } from '../service/authentication.service';
import { User } from '../model/user';

@Injectable({
  providedIn: 'root'
})
export class RegistrationService {
  baseurl = environment.url;

  constructor(private authenticationService:AuthenticationService,private http: HttpClient, private router: Router) {}

  register(user: InsertUserDTO) {
    return this.http.post(this.baseurl + 'auth/register', user);
  }

  sendToken(token: string) {
    this.http.post(this.baseurl + 'auth/conform-account', {token}, {
    }).subscribe(data => {
      
      this.router.navigate(['login']); 
    },
      error => {
      });
  }

}
