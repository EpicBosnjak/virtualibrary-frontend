import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment} from '../../environments/environment';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { Category } from '../model/category';
import { APIErrorResponse } from '../model/APIError';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  baseURL = environment.url;
  dataCategories: BehaviorSubject<Category[]> = new BehaviorSubject<Category[]>([]);
  

  constructor(private http: HttpClient, private router: Router) { }

  public getCategories(): Observable<Category[]>{

    this.http.get<Category[]>(this.baseURL+"categories").subscribe(data =>{
      this.dataCategories.next(data);
    },(error: APIErrorResponse) => {
      alert(error.error.message)
    });
    return this.dataCategories.asObservable();
  }

  dataCategory: BehaviorSubject<Category> = new BehaviorSubject<Category>(null);
  
  public insert(name:string): Observable<Category>{
    this.http.post<Category>(this.baseURL + "categories",{"name":name}).subscribe(data=>{
      this.dataCategory.next(data);
    }, (error: APIErrorResponse) => {
      var message = '['+ error.error.status + '] ' +error.error.message;
      alert(message);
    })

    return this.dataCategory.asObservable();
  }

}
