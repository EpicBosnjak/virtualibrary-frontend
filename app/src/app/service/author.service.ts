import { Injectable } from '@angular/core';
import { APIErrorResponse } from '../model/APIError';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { environment } from './../../environments/environment';

import { SimpleAuthor } from '../model/author';

@Injectable({
  providedIn: 'root'
})
export class AuthorService {

  baseURL = environment.url;
  dataAuthors: BehaviorSubject<SimpleAuthor[]> = new BehaviorSubject<SimpleAuthor[]>([]);
  dataAuthor: BehaviorSubject<SimpleAuthor> = new BehaviorSubject<SimpleAuthor>(null);

  constructor(private http: HttpClient) { }


  public getAuthors(): Observable<SimpleAuthor[]> {
    this.http.get<SimpleAuthor[]>(this.baseURL + "authors")
      .subscribe(response => {

        this.dataAuthors.next(response);

      },
        (error: APIErrorResponse) => {
          var message = '[' + error.error.status + '] ' + error.error.message;
          alert(message);
        });

        return this.dataAuthors.asObservable();
  }

  public insert(name:string,lastname:string): Observable<SimpleAuthor>{
    this.http.post<SimpleAuthor>(this.baseURL + "authors",{"firstName":name,"lastName":lastname}).subscribe(data=>{
      this.dataAuthor.next(data);
    }, (error: APIErrorResponse) => {
      var message = '['+ error.error.status + '] ' +error.error.message;
      alert(message);
    })

    return this.dataAuthor.asObservable();

  }

}
