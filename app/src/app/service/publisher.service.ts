import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment} from '../../environments/environment';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { Publisher } from '../model/publisher';
import { APIErrorResponse } from '../model/APIError';

@Injectable({
  providedIn: 'root'
})
export class PublisherService {

  baseURL = environment.url;
  dataPublishers: BehaviorSubject<Publisher[]> = new BehaviorSubject<Publisher[]>([]);

  constructor(private http: HttpClient) { }

  public getPublishers(): Observable<Publisher[]>{

    this.http.get<Publisher[]>(this.baseURL+"publishers").subscribe(data =>{
      this.dataPublishers.next(data);
    },(error: APIErrorResponse) => {
      alert(error.error.message)
    });
    return this.dataPublishers.asObservable();
  }

  dataPublisher: BehaviorSubject<Publisher> = new BehaviorSubject<Publisher>(null);
  
  public insert(name:string): Observable<Publisher>{
    this.http.post<Publisher>(this.baseURL + "publishers",{"name":name}).subscribe(data=>{
      this.dataPublisher.next(data);
    }, (error: APIErrorResponse) => {
      var message = '['+ error.error.status + '] ' +error.error.message;
      alert(message);
    })

    return this.dataPublisher.asObservable();
  }

}
