import { Injectable } from '@angular/core';
import { APIErrorResponse } from '../model/APIError';

import { Router  } from '@angular/router';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from './../../environments/environment';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { AuthenticationService } from './authentication.service';
import { BookCopy, SimpleBookCopy } from '../model/bookCopy'
@Injectable({
  providedIn: 'root'
})
export class BookCopyService {
  baseURL = environment.url;
  dataBookCopy: BehaviorSubject<BookCopy> = new BehaviorSubject<BookCopy>(null);
  dataSimpleBookCopiesList: BehaviorSubject<SimpleBookCopy[]> = new BehaviorSubject<SimpleBookCopy[]>([]);


  constructor(private router: Router,private http: HttpClient, private authService: AuthenticationService) { }

  public insertBookCopy(id: number): Observable<BookCopy> {
    this.http.post<any>(this.baseURL + 'book-copies/', { bookId: id, userId: this.authService.currentUserValue.id })
      .subscribe(response => {
        this.dataBookCopy.next(response);
        this.router.navigate(['/read',id]);
        this.authService.currentUserValue.bookmark-=1;
      },
        (error: APIErrorResponse) => {
          var message = '[' + error.error.status + '] ' + error.error.message;
          alert(message);
        });
    return this.dataBookCopy.asObservable();

  }

  public getBookCopiesForUser(id: number): Observable<SimpleBookCopy[]> {
    this.http.get<SimpleBookCopy[]>(this.baseURL + "book-copies/user/" + id).subscribe(responseData => {
      this.dataSimpleBookCopiesList.next(responseData);
    },
      (error: APIErrorResponse) => {
        var message = '[' + error.error.status + '] ' + error.error.message;
        //alert(message);
      });
    return this.dataSimpleBookCopiesList.asObservable();
  }

  public getBookCopy(id: any): Observable<BookCopy> {
    this.http.get<BookCopy>(this.baseURL + "book-copies/" + id).subscribe(responseData => {
      this.dataBookCopy.next(responseData);
    }, (error: APIErrorResponse) => {
      var message = '[' + error.error.status + '] ' + error.error.message;
      //alert(message);
    });

    return this.dataBookCopy.asObservable();
  }

  public page(id: number, currentPage: number): Observable<BookCopy> {
    this.http.put<BookCopy>(this.baseURL + "book-copies/" + id + "/page/" + currentPage,null)
      .subscribe(responseData => {
        this.dataBookCopy.next(responseData);
      }, (error: APIErrorResponse) => {
        var message = '[' + error.error.status + '] ' + error.error.message;
        //alert(message);
      });
    return this.dataBookCopy.asObservable();
  }

}
