/////////////////////////IMPORTS > module////////////////////////////////
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MaterialModule} from './material-module';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; 
import { HttpClientModule} from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxDropzoneModule } from 'ngx-dropzone';
/////////////////////////////////////////////////////////////////////////

////////////////////// DECLARATIONS > Component//////////////////////////
import { AppComponent } from './app.component';
import { LoginComponent } from './component/login/login.component';
import { HeaderComponent } from './component/header/header.component';
import { SimpleBookListComponent } from './component/simple-book-list/simple-book-list.component';
import { RegistrationComponent } from './component/registration/registration.component';
import { ProfileComponent } from './component/profile/profile.component';
import { HomepageComponent } from './component/homepage/homepage.component';
import { BookDetailedComponent} from './component/book-detailed/book-detailed.component';
import { AdminPageComponent } from './component/admin-page/admin-page.component';

/////////////////////////////////////////////////////////////////////////

////////////////////////PROVIDERS > service//////////////////////////////
import { AuthenticationService } from './service/authentication.service';
import { SimpleBookService } from './service/simple-book.service';
import { CategoryService } from './service/category.service'

import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { JwtInterceptor} from './_helpers/jwt.iterceptor';
import { ErrorInterceptor} from './_helpers/error.interceptor';
import { BookReaderComponent } from './component/book-reader/book-reader.component';
import { BookCopyService } from './service/book-copy.service';
import { BookmarkDialogComponent, AuthorDialogComponent,CategoryDialogComponent,PublisherDialogComponent } from './component/dialog/dialog.component';



///////////////////////////////////////////////////////////////////////


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HeaderComponent,
    SimpleBookListComponent,
    RegistrationComponent,
    ProfileComponent,
    HomepageComponent,
    BookDetailedComponent,
    BookReaderComponent,
    AdminPageComponent,
    AuthorDialogComponent,
    CategoryDialogComponent,
    PublisherDialogComponent,
    BookmarkDialogComponent
  ],
  entryComponents: [
    AuthorDialogComponent,
    CategoryDialogComponent,
    PublisherDialogComponent,
    BookmarkDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MaterialModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ToastrModule.forRoot(),
    ReactiveFormsModule,
    FormsModule,
    NgxDropzoneModule
  ],
  providers: [
      { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
      { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    AuthenticationService,
    SimpleBookService,
    CategoryService,
    BookCopyService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
