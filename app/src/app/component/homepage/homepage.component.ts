import { Component, OnInit } from '@angular/core';

import { CategoryService } from '../../service/category.service';
import { BookService } from '../../service/book.service';

import { SimpleBook } from '../../model/book';
import { Category } from 'src/app/model/category';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {
  showCategories = true;
  categories: Category[] = new Array();
  books :SimpleBook[] = new Array(); 
  searchText:string;
  constructor( private categoryService: CategoryService,private bookService: BookService) { }
  


  ngOnInit() {
    this.initCategory();
    this.initBooks();
  }

  initCategory(){
    this.categoryService.getCategories().subscribe(data =>{
      this.categories=data;
      this.categories.sort((a,b) => a.name.localeCompare(b.name));
    });
  }

  initBooks(){
    this.bookService.getBooks().subscribe(data => {
      this.books=data;
    });
  }

  onClick(categoryId:number){
    this.bookService.filter(categoryId).subscribe(data => {
      this.books=data;
    });
  }

  applyFilter(filterValue: string){
    this.books = this.books.filter(book=>book.name==filterValue);
  }
}
