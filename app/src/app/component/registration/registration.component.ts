import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { RegistrationService } from '../../service/registration.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { InsertUserDTO } from 'src/app/model/dto/insertUserDTO';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  registrationForm: FormGroup;errorMessage: string = null;
  successfulReg: boolean = false;
  success: string = null;
  user: InsertUserDTO;

  constructor(private registrationService: RegistrationService,
    private router: Router,private toastr: ToastrService) { }

  ngOnInit() {
    this.registrationForm = new FormGroup({
      'firstName': new FormControl('', Validators.required),
      'lastName': new FormControl('', Validators.required),
      'email': new FormControl('', [Validators.required, Validators.email]),
      'password': new FormControl('', [Validators.required, Validators.minLength(6)]),
      'passwordRepeat': new FormControl('', [Validators.required, Validators.minLength(6)])
    });
  }
  onSubmit() {
    if (this.registrationForm.invalid) {
      this.errorMessage = "Please enter a valid data.";
      this.toastr.error(  this.errorMessage, 'Registration');
      return;
    }
    
    if (!(this.registrationForm.value.password === this.registrationForm.value.passwordRepeat)) {
      this.errorMessage = "Password must match in both fields.";
      this.toastr.error(  this.errorMessage, 'Registration');
      return;
    }

    const user = new InsertUserDTO(this.registrationForm.value.firstName,
      this.registrationForm.value.lastName,
      this.registrationForm.value.email,
      this.registrationForm.value.password);

    this.registrationService.register(user).subscribe(
      responseData => {
        this.errorMessage = null;
        this.successfulReg = true;
        this.registrationForm.reset();
        this.success = "You need to verify your account. Please check your email.";
      });
    }

    canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
      if (!this.successfulReg &&
        (this.registrationForm.value.firstName !== '' ||
          this.registrationForm.value.lastName !== '' || this.registrationForm.value.email !== ''
          || this.registrationForm.value.password !== '' || this.registrationForm.value.passwordRepeat !== '')) {
        return confirm('Are you sure you want to cancel registration?');
      } else {
        return true;
      }
    }
  
    login() {
      this.router.navigate(['/login']);
    }

}
