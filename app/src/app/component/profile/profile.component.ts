import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/model/user';
import { AuthenticationService } from '../../service/authentication.service';
import { BookCopyService } from '../../service/book-copy.service';
import { BookCopy, SimpleBookCopy } from '../../model/bookCopy';
import { Router} from '@angular/router'
import { MatDialog} from '@angular/material/dialog';

import { BookmarkDialogComponent} from '../../component/dialog/dialog.component';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  user: User;
  simpleBookCopiesList = new Array<SimpleBookCopy>();

  show:boolean=false;
  constructor(private authService: AuthenticationService,
              private bookCopyService: BookCopyService,
              private router: Router,
              public dialog: MatDialog) { }

  ngOnInit() {
    this.user=this.authService.currentUserValue;
    this.bookCopyService.getBookCopiesForUser(this.user.id).subscribe(data =>{
      this.simpleBookCopiesList = data;
      this.show=true;
    })
  }

  read(id: number){
    console.log(id);
    this.router.navigate(['read/'+id]);
  }

  openBookmarkDialog(): void {
    const dialogRef = this.dialog.open(BookmarkDialogComponent, {
      width: '400px'
    });

    dialogRef.afterClosed().subscribe(result => {
      this.ngOnInit();
    });
  }

}
