import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,Router  } from '@angular/router';
import { BookService } from '../../service/book.service'
import { Book } from '../../model/book';
import { Location} from '@angular/common';
import { BookCopyService } from '../../service/book-copy.service';
@Component({
  selector: 'app-book-detailed',
  templateUrl: './book-detailed.component.html',
  styleUrls: ['./book-detailed.component.css']
})
export class BookDetailedComponent implements OnInit {
  book: Book;
  id: string;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private bookService:BookService,
              private location:Location,
              private bookCopyService:BookCopyService) { }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.bookService.getBook(this.id).subscribe(data => {
      this.book = data;
    });
  }

  back(){
    this.location.back();
  }

  rent(){
    console.log("Rent this book" + this.book.id);
    this.bookCopyService.insertBookCopy(this.book.id).subscribe(data =>{
      
    });
    
  }
}
