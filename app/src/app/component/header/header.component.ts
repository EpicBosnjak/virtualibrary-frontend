import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { AuthenticationService } from '../../service/authentication.service'
import { User } from 'src/app/model/user';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  user: User;

  constructor(private router: Router, private authenticationService:AuthenticationService ) { }

  ngOnInit() {
    
  }

  isLoggedIn() {
    this.user = this.authenticationService.currentUserValue;
    return this.authenticationService.isLoggedIn();
  }

  isAdmin() {
    return this.authenticationService.isAdmin();
  }

  onLogout() {
    this.authenticationService.logout();
    this.router.navigate(['/login']);
  }
}
