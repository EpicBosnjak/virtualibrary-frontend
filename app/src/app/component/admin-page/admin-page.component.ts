import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SelectionModel } from '@angular/cdk/collections';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';


import { AuthorService } from '../../service/author.service';
import { SimpleAuthor } from '../../model/author';

import { CategoryService } from '../../service/category.service';
import { Category } from '../../model/category';

import { PublisherService } from '../../service/publisher.service';
import { Publisher } from '../../model/publisher';

import { InsertBookDTO } from '../../model/dto/insertBookDTO';
import { BookService } from '../../service/book.service';

import { AuthorDialogComponent, PublisherDialogComponent, CategoryDialogComponent } from '../dialog/dialog.component';




@Component({
  selector: 'app-admin-page',
  templateUrl: './admin-page.component.html',
  styleUrls: ['./admin-page.component.css']
})
export class AdminPageComponent implements OnInit {


  constructor(private _formBuilder: FormBuilder,
    private authorService: AuthorService,
    private categoryService: CategoryService,
    private publisherService: PublisherService,
    private bookService: BookService,
    public dialog: MatDialog) { }


  openCategoryDialog(): void {
    const dialogRef = this.dialog.open(CategoryDialogComponent, {
      width: '400px',
      data: { data2:this.dataSourceAuthors}
    });

    dialogRef.afterClosed().subscribe(result => {
      this.initCategory();
      
    });
  }

  openAuthorDialog():void{
    let dialogRef = this.dialog.open(AuthorDialogComponent, {
      width: '400px'
    });

    dialogRef.afterClosed().subscribe(result => {
      this.initAuthors();
      
    });

  }

  openPublisherDialog():void{
    let dialogRef = this.dialog.open(PublisherDialogComponent, {
      width: '400px'
    });

    dialogRef.afterClosed().subscribe(result => {
      this.initPublisher();
      
    });

  }



ngOnInit() {
  this.init();
}

init() {
  this.initBookForm();
  this.initAuthors();
  this.initCategory();
  this.initPublisher();

}
///////////////////////////////////////////////////////////////////////////
//----------------------------BOOK INFO----------------------------------//
///////////////////////////////////////////////////////////////////////////
bookInfo: FormGroup;

initBookForm() {
  this.bookInfo = this._formBuilder.group({
    bookName: ['', Validators.required],
    bookISBN: ['', Validators.required],
    bookPublishedYear: ['', Validators.required],
    bookDescription: ['', Validators.nullValidator]
  });
}


///////////////////////////////////////////////////////////////////////////
//----------------------------AUTHOR-------------------------------------//
///////////////////////////////////////////////////////////////////////////
authors: SimpleAuthor[];
displayedColumnsAuthors: string[] = ['select', 'firstName', 'lastName'];
dataSourceAuthors: MatTableDataSource<SimpleAuthor>;
selectionAuthors = new SelectionModel<SimpleAuthor>(true, []);

initAuthors() {
  this.authorService.getAuthors().subscribe(data => {
    this.authors = data;
    this.dataSourceAuthors = new MatTableDataSource<SimpleAuthor>(this.authors);
    console.log(this.authors);
  });
}


isAllSelectedAuthors() {
  const numSelected = this.selectionAuthors.selected.length;
  const numRows = this.dataSourceAuthors.data.length;
  return numSelected === numRows;
}

masterToggleAuthors() {
  this.isAllSelectedAuthors() ?
    this.selectionAuthors.clear() :
    this.dataSourceAuthors.data.forEach(row => this.selectionAuthors.select(row));
}

checkboxLabelAuthors(row ?: SimpleAuthor): string {
  if (!row) {
    return `${this.isAllSelectedAuthors() ? 'select' : 'deselect'} all`;
  }
  return `${this.selectionAuthors.isSelected(row) ? 'deselect' : 'select'} row ${row.id}`;
}

applyAuthorsFilter(filterValue: string) {
  this.dataSourceAuthors.filter = filterValue.trim().toLowerCase();
}
///////////////////////////////////////////////////////////////////////////
//----------------------------CATEGRY------------------------------------//
///////////////////////////////////////////////////////////////////////////

categories: Category[];
displayedColumnsCategory: string[] = ['select', 'name'];
dataSourceCategory: MatTableDataSource<Category>;
selectionCategory = new SelectionModel<Category>(true, []);

initCategory(){
  this.categoryService.getCategories().subscribe(data => {
    this.categories = data;
    this.dataSourceCategory = new MatTableDataSource<Category>(this.categories);
  });
}

isAllSelectedCategory() {
  const numSelected = this.selectionCategory.selected.length;
  const numRows = this.dataSourceCategory.data.length;
  return numSelected === numRows;
}

masterToggleCategory() {
  this.isAllSelectedCategory() ?
    this.selectionCategory.clear() :
    this.dataSourceCategory.data.forEach(row => this.selectionCategory.select(row));
}

checkboxLabelCategory(row ?: Category): string {
  if (!row) {
    return `${this.isAllSelectedCategory() ? 'select' : 'deselect'} all`;
  }
  return `${this.selectionCategory.isSelected(row) ? 'deselect' : 'select'} row ${row.id}`;
}

applyCategoryFilter(filterValue: string) {
  this.dataSourceCategory.filter = filterValue.trim().toLowerCase();
}

///////////////////////////////////////////////////////////////////////////
//---------------------------PUBLISHER-----------------------------------//
///////////////////////////////////////////////////////////////////////////

publishers: Publisher[];
displayedColumnsPublisher: string[] = ['select', 'name'];
dataSourcePublisher: MatTableDataSource<Publisher>;
selectionPublisher = new SelectionModel<Publisher>(false, []);

initPublisher(){
  this.publisherService.getPublishers().subscribe(data => {
    this.publishers = data;
    this.dataSourcePublisher = new MatTableDataSource<Publisher>(this.publishers);
  })
}

isAllSelectedPublisher() {
  const numSelected = this.selectionPublisher.selected.length;
  const numRows = this.dataSourcePublisher.data.length;
  return numSelected === numRows;
}

masterTogglePublisher() {
  this.isAllSelectedPublisher() ?
    this.selectionPublisher.clear() :
    this.dataSourcePublisher.data.forEach(row => this.selectionPublisher.select(row));
}

checkboxLabelPublisher(row ?: Publisher): string {
  if (!row) {
    return `${this.isAllSelectedPublisher() ? 'select' : 'deselect'} all`;
  }
  return `${this.selectionPublisher.isSelected(row) ? 'deselect' : 'select'} row ${row.id}`;
}

applyPublisherFilter(filterValue: string) {
  this.dataSourcePublisher.filter = filterValue.trim().toLowerCase();
}
///////////////////////////////////////////////////////////////////////////
//---------------------------------PDF-----------------------------------//
///////////////////////////////////////////////////////////////////////////
files: File[] = [];
onSelect(event) {
  console.log(event);
  this.files.push(...event.addedFiles);
}

onRemove(event) {
  console.log(event);
  this.files.splice(this.files.indexOf(event), 1);
}
get f() { return this.bookInfo.controls; }

onDone(){
  let insertBook = new InsertBookDTO(this.f.bookISBN.value, this.f.bookName.value, this.f.bookPublishedYear.value, this.f.bookDescription.value);

  for (let a of this.selectionAuthors.selected) {
    insertBook.addAuthor(a.id);
  }
  for (let c of this.selectionCategory.selected) {
    insertBook.addCategory(c.id);
  }

  for (let p of this.selectionPublisher.selected) {
    insertBook.addPublisher(p.id);
  }

  if (insertBook.valid) {
    console.log(insertBook);
    this.bookService.insertBook(insertBook).subscribe(data => {
      console.log(data);
      this.uploadFile(data.id);
    })
  }
}
uploadFile(id: number){
  const formData = new FormData();
  formData.append('file', this.files[0]);
  this.bookService.insertPDF(id, formData);
}


  
}
