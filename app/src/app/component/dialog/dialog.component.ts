import { Component, Input,Inject } from '@angular/core';
import { MatDialogRef,MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AuthorService } from '../../service/author.service';
import { MatTableDataSource } from '@angular/material/table';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CategoryService } from '../../service/category.service'; 
import { PublisherService } from '../../service/publisher.service';
import { AuthenticationService } from '../../service/authentication.service';

import {SimpleAuthor} from '../../model/author';

@Component({
  selector: 'app-author-dialog',
  templateUrl: './template/author.template.html'
})
export class AuthorDialogComponent {
  insertAuthor:FormGroup=this.formBuilder.group({
    firstName: ['', Validators.required],
    lastName: ['', Validators.required]
  });
  constructor(
    private formBuilder: FormBuilder,
    private authorService:AuthorService,
    public dialogRef: MatDialogRef<AuthorDialogComponent>) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  get f() { return this.insertAuthor.controls; }
  onClick(){
     
    this.authorService.insert(this.f.firstName.value,this.f.lastName.value).subscribe(response => {
      this.onNoClick();
    })
  }
}

@Component({
  selector: 'app-author-dialog',
  templateUrl: './template/category.template.html'
})
export class CategoryDialogComponent {

  insertCategory:FormGroup=this.formBuilder.group({
    name: ['', Validators.required]
  });

  constructor(
    private categoryService:CategoryService,
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<CategoryDialogComponent>) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  get f() { return this.insertCategory.controls; }
  onClick(){
     
    this.categoryService.insert(this.f.name.value).subscribe(response => {
      this.onNoClick();
    })
  }
}



@Component({
  selector: 'app-author-dialog',
  templateUrl: './template/publisher.template.html'
})
export class PublisherDialogComponent {

  insertPublisher:FormGroup=this.formBuilder.group({
    name: ['', Validators.required]
  });

  constructor(private publisherService:PublisherService,
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<PublisherDialogComponent>) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  get f() { return this.insertPublisher.controls; }
  onClick(){
     
    this.publisherService.insert(this.f.name.value).subscribe(response => {
      this.onNoClick();
    })
  }
}


@Component({
  selector: 'app-author-dialog',
  templateUrl: './template/bookmark.template.html'
})
export class BookmarkDialogComponent {
  insertBookmark:FormGroup=this.formBuilder.group({
    insertBookmark: ['', Validators.required],
  });
  constructor(
    private formBuilder: FormBuilder,
    private authService:AuthenticationService,
    public dialogRef: MatDialogRef<BookmarkDialogComponent>) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  get f() { return this.insertBookmark.controls; }
  onClick(){
    
    this.authService.insertBookmarks(this.f.insertBookmark.value).subscribe(response => {
      this.onNoClick();
    })
  }
}