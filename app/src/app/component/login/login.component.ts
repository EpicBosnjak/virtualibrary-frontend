import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { AuthenticationService } from '../../service/authentication.service';
import { RegistrationService } from '../../service/registration.service'


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  token: string;
  submitted = false;
  constructor(private formBuilder: FormBuilder, private authenticationService: AuthenticationService, 
              private router: Router, private route: ActivatedRoute,
              private registrationService: RegistrationService) {
    if (this.authenticationService.currentUserValue) {
      this.router.navigate(['/']);
    }
  }

  ngOnInit() {
    if (this.authenticationService.isLoggedIn()) this.router.navigate(['/']);

    this.loginForm = this.formBuilder.group({
      email: ['', Validators.email],
      password: ['', Validators.required]
    });
    this.route.queryParams.subscribe(
      (queyParams: Params) => {
        this.token = queyParams['token'];
        if (this.token != undefined) {
          this.registrationService.sendToken(this.token);
        }
      }
    );
  }

  get f() { return this.loginForm.controls; }

  onSubmit() {
    this.submitted = true;
    if (this.loginForm.invalid) {
      return;
    }
    this.authenticationService.login(this.f.email.value, this.f.password.value)
      .pipe(first())
      .subscribe(
        data => {
          this.router.navigate(['/']);
        });
  }

  register() {
    this.router.navigate(['/register']);
  }

}
