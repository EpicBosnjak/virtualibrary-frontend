import { Component, OnInit } from '@angular/core';
import { Location} from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { BookCopyService } from '../../service/book-copy.service';
import { BookCopy } from '../../model/bookCopy';

@Component({
  selector: 'app-book-reader',
  templateUrl: './book-reader.component.html',
  styleUrls: ['./book-reader.component.css']
})
export class BookReaderComponent implements OnInit {
  show: boolean = false;

  id: string;
  bookCopy: BookCopy;
  curentPage: number;

  constructor(private route: ActivatedRoute,
              private location:Location,
              private bookCopyService:BookCopyService){}

  ngOnInit(){
    this.id = this.route.snapshot.paramMap.get('id');
    this.bookCopyService.getBookCopy(this.id).subscribe(data => {
      this.bookCopy=data;
      console.log(this.bookCopy);
      this.curentPage= this.bookCopy.currentPage;
      this.show = true;
    });
    
  }

  public previousPage(){
    console.log("previousPage")
    if(this.valid(this.curentPage-1)){
      this.bookCopyService.page(this.bookCopy.id,this.curentPage-1).subscribe(
        data => {
          this.bookCopy=data;
          this.curentPage=this.bookCopy.currentPage;
        }
      );
    }
  }

  public nextPage(){
    console.log("nextPage")
    if(this.valid(this.curentPage+1)){
      this.bookCopyService.page(this.bookCopy.id,this.curentPage+1).subscribe(
        data => {
          this.bookCopy=data;
          this.curentPage=this.bookCopy.currentPage;
        }
      );
    }
  }

  valid(page:number):boolean{
    console.log("valid")
    return page>0 && page <this.bookCopy.pageNumber;
  }

    onUpdate(event: Event) {   
      if(this.valid(Number((<HTMLInputElement>event.target).value))){
        this.bookCopyService.page(this.bookCopy.id,Number((<HTMLInputElement>event.target).value)).subscribe(
          data => {
            this.bookCopy=data;
            this.curentPage=this.bookCopy.currentPage;
          }
        );
      }
  }

}
