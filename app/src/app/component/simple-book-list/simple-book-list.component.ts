import { Component,Input, OnInit } from '@angular/core';
import { BookService } from '../../service/book.service';
import { SimpleBook } from '../../model/book';



@Component({
  selector: 'app-simple-book-list',
  templateUrl: './simple-book-list.component.html',
  styleUrls: ['./simple-book-list.component.css']
})
export class SimpleBookListComponent implements OnInit {

  //books:SimpleBook[] = new Array(); 
  
  @Input()books:SimpleBook[];
  constructor(private bookService: BookService) { }

  ngOnInit() {
    this.bookService.getBooks().subscribe(data => {
      this.books=data;
    });
    


  }

}
