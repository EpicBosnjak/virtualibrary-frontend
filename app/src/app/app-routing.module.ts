import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuard}  from './_helpers/auth.guard';
import { AdminGuard } from './_helpers/admin.guard';

import { LoginComponent}  from './component/login/login.component'
import { RegistrationComponent } from './component/registration/registration.component';
import { HomepageComponent } from './component/homepage/homepage.component';
import { BookDetailedComponent } from './component/book-detailed/book-detailed.component';
import { ProfileComponent } from './component/profile/profile.component';
import { BookReaderComponent } from './component/book-reader/book-reader.component';
import { AdminPageComponent } from './component/admin-page/admin-page.component';
const routes: Routes = [
  { path:'login'         , component: LoginComponent },
  { path:'register'      , component: RegistrationComponent },
  /////////////USER/////////////,canActivate: [AuthGuard]
  { path: ''             , component: HomepageComponent         , canActivate: [AuthGuard] },
  { path: 'books/:id'    , component: BookDetailedComponent     , canActivate: [AuthGuard] },
  { path: 'profile'      , component: ProfileComponent          , canActivate: [AuthGuard] },
  { path: 'read/:id'     , component: BookReaderComponent       , canActivate: [AuthGuard] },
  { path: 'admin'        , component: AdminPageComponent        , canActivate: [AuthGuard] },
  /////////////ADMIN/////////////
  //{ path: 'admin'        , component: AdminPageComponent        , canActivate: [AdminGuard] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
