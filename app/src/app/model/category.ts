import { SimpleCategoryLinks } from './links/links';

export class Category {
    id: number;
    name: string;
    links: SimpleCategoryLinks;

    constructor(id: number, name: string, links: SimpleCategoryLinks) {
        this.id = id;
        this.name = name;
        this.links = links;
    }
}