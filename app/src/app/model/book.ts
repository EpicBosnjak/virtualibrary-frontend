import {SimpleBookLinks, BookLinks} from './links/links';


export class Book{
    id: number;
    isbn: string;
    name: string;
    description: string;
    publishedYear: number;
    pages:number;
    links: BookLinks;

    constructor(id: number, isbn: string,name: string,description: string,publishedYear: number,pages:number,links: BookLinks){
        this.id=id;
        this.isbn=isbn;
        this.name=name;
        this.description=description;
        this.publishedYear=publishedYear;
        this.pages=pages;
        this.links = links;

    }
}

export class SimpleBook{
    id: number;
    name:string;
    links: SimpleBookLinks;

    constructor(id: number, name: string, links:SimpleBookLinks){
        this.id = id;
        this.name = name;
        this.links = links;
    }
    
}

