import { SimpleAuthorLinks } from './links/links';

export class SimpleAuthor{
    id: number;
    firstName: string;
    lastName: string;
    links: SimpleAuthorLinks;

    constructor(id: number, firstName: string, lastName: string, links: SimpleAuthorLinks){
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.links = links;
    }
}

