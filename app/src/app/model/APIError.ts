import { HttpErrorResponse } from "@angular/common/http"

export interface APIErrorResponse extends HttpErrorResponse {
    error:{
        timestamp:string
        status: number
        error: string
        message:string[]
        path:string
    }
}