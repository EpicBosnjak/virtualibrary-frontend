import { PublisherLinks } from './links/links';

export class Publisher{
    id: number;
    name: string;
    links: PublisherLinks;

    constructor(id: number, name: string, links: PublisherLinks){
        this.id=id;
        this.name=name;
        this.links=links;
    }
}