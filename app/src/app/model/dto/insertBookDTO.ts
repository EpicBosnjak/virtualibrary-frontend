export class InsertBookDTO{
    isbn: string;
    name: string;
    publishedYear: number;
    description: string;
    authors: number[] = new Array();
    categories: number[] = new Array();
    publisher:number = null;

    constructor(isbn: string, name: string, publishedYear: number, description: string){
        this.isbn = isbn;
        this.name = name;
        this.publishedYear = publishedYear;
        this.description = description;
    }

    public addAuthor(id:number){
        this.authors.push(id);
    }

    public addCategory(id:number){
        this.categories.push(id);
    }

    public addPublisher(id:number){
        this.publisher = id;
    }

    public valid():boolean{
        if(this.isbn!=null){
            return false;
        }
        if(this.name!=null){
            return false;
        }
        if(this.publishedYear!=null){
            return false;
        }
        if(this.categories.length==0){
            return false;
        }
        return true;
    }
}