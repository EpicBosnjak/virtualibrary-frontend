export class User{
    id: number;
    bookmark: number;
    sub: string;
    name: string;
    email: string;
    role: string;
    token?: string;

    constructor(id: number,bookmark: number,sub: string,name: string,email: string,role: string,token?: string){
        this.id = id;
        this.bookmark = bookmark;
        this.sub = sub;
        this.name = name;
        this.email = email;
        this.role = role;
        this.token = token;
    }
}