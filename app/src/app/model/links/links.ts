
///////////////////////////////BOOOK/////////////////////////////////////
export class BookLinks{
    images:LinkHref[];
    self:LinkHref[];
    publisher:LinkHref[];
    categories:LinkHrefAndName[];
    authors:LinkHrefAndName[];
    constructor(images:LinkHref[],self:LinkHref[],publisher:LinkHref[],categories:LinkHrefAndName[], authors:LinkHrefAndName[]){
        this.images = images;
        this.self = self;
        this.publisher = publisher;
        this.categories = categories;
        this.authors = authors;
    }
}

export class SimpleBookLinks{
    images:LinkHref[];
    self:LinkHref[];
    authors:LinkHrefAndName[];

    constructor(images:LinkHref[], self:LinkHref[],authors:LinkHrefAndName[]){
        this.images = images;
        this.self = self;
        this.authors = authors;
    }
}

////////////////////////////////////////////////////////////////////////
///////////////////////////////BOOK COPY////////////////////////////////
export class BookCopyLinks{
    books:LinkHrefAndName[];
    self:LinkHref[];
    users:LinkHrefAndName[];

    constructor(books:LinkHrefAndName[],self:LinkHref[],users:LinkHrefAndName[]){
        this.books = books;
        this.self = self;
        this.users = users;
    }
}
export class SimpleBookCopyLinks{
    self:LinkHref[];
    books:LinkHrefAndName[];
    constructor(self:LinkHref[],books:LinkHrefAndName[]){
        this.self=self;
        this.books = books;
    }
}
////////////////////////////////////////////////////////////////////////
/////////////////////////////CATEGORY///////////////////////////////////


export class SimpleCategoryLinks{
    self:LinkHref[];

    constructor(self:LinkHref[]){
        this.self=self;
    }
}



/////////////////////////////////////////////////////////////////////////
/////////////////////////////CATEGORY///////////////////////////////////

export class SimpleAuthorLinks{
    self:LinkHref[];

    constructor(self:LinkHref[]){
        this.self=self;
    }
}

/////////////////////////////////////////////////////////////////////////
/////////////////////////////PUBLISHER///////////////////////////////////

export class PublisherLinks{
    self:LinkHref[];

    constructor(self:LinkHref[]){
        this.self=self;
    }
}


/////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////
 class LinkHref{
    href:string;
    constructor(href:string){
        this.href = href
    }
}

 class LinkHrefAndName{
    name:string;
    href:string;

    constructor(name:string, href:string){
        this.name=name;
        this.href = href;
    }
}

