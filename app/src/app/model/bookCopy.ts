import { BookCopyLinks,SimpleBookCopyLinks } from '../model/links/links'

export class BookCopy{
    id: number;
    currentPage: number;
    pageNumber:number;
    rentDate: Date;
    expiryDate: Date;
    path:string;
    
    links:BookCopyLinks;

    constructor(id: number, currentPage: number, pageNumber:number, rentDate: Date, expiryDate: Date,path:string, links:BookCopyLinks){
        this.id=id;
        this.currentPage=currentPage;
        this.pageNumber = pageNumber;
        this.rentDate=rentDate;
        this.expiryDate = expiryDate;
        this.path=path;
        this.links = links;
    }
}

export class SimpleBookCopy{
    id:number;
    links:SimpleBookCopyLinks;
    constructor(id:number,links:SimpleBookCopyLinks){
        this.id = id;
        this.links=links;
    }
}